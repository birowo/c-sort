package sort

import (
	"sync"
)

func partition(start, end int, less func(int, int) bool, swap func(int, int)) int {
	for i := start; i < end; i++ {
		if less(i, end) {
			swap(i, start)
			start++
		}
	}
	swap(start, end)
	return start
}
func Quick(slcLen int, less func(int, int) bool, swap func(int, int)) {
	type Part struct {
		start, end int
	}
	parts := make([]Part, slcLen)
	parts[0] = Part{0, slcLen - 1}
	i, j := 0, 1
	var wg sync.WaitGroup
	mu := &sync.Mutex{}
	for k := 1; k != 0; k = j - i {
		//print(k, " ")
		if k == 1 {
			part := parts[i]
			i = j
			pivot := partition(part.start, part.end, less, swap)
			if pivot-1 > part.start {
				parts[j] = Part{part.start, pivot - 1}
				j++
			}
			if pivot+1 < part.end {
				parts[j] = Part{pivot + 1, part.end}
				j++
			}
		} else {
			partsIJ := parts[i:j]
			i = j
			wg.Add(k)
			for _, part := range partsIJ {
				go func(part Part) {
					pivot := partition(part.start, part.end, less, swap)
					mu.Lock()
					if pivot-1 > part.start {
						parts[j] = Part{part.start, pivot - 1}
						j++
					}
					if pivot+1 < part.end {
						parts[j] = Part{pivot + 1, part.end}
						j++
					}
					mu.Unlock()
					wg.Done()
				}(part)
			}
			wg.Wait()
		}
		//println(j)
	}
}