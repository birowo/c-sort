# sort

#GOLANG concurrent quick sort

**install:** `go get -u gitlab.com/birowo/c-sort`

**reference:** [iterative implementation of quicksort](https://www.techiedelight.com/iterative-implementation-of-quicksort/)